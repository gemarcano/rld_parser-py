#!/usr/bin/python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import SpanSelector
import pudb
from rld import RLD
from functools import partial


def extract_all_rld_data(rld: RLD):
    """Extracts all RLD data from the given file into memory"""

    start_time = rld.metadata["start_time_s"] + rld.metadata["start_time_ns"] * 10**-9

    count = 0

    # Every block is about one second
    data = {name[:2]: [] for name in rld.channels["analog"]}
    binary_data = {name[:3]: [] for name in rld.channels["binary"] if 'valid' not in name}
    data = data | binary_data
    data["time"] = []
    for block in rld:
        count += 1

        current_time = block["time"] - start_time
        if np.any(current_time < 0):
            pudb.set_trace()
            continue

        data["time"].extend(current_time)

        for name in data.keys():
            if name[0] == "I":
                i_l = (
                    block["analog"][name + "L"]
                    * 10 ** rld.channels["analog"][name + "L"]["scale"]
                    * block["binary"][name + "L_valid"]
                )
                i_h = (
                    block["analog"][name + "H"]
                    * 10 ** rld.channels["analog"][name + "H"]["scale"]
                    * (1 - block["binary"][name + "L_valid"])
                )
                data[name].extend(i_l + i_h)
            elif name[0] == "V":
                data[name].extend(
                    block["analog"][name] * 10 ** rld.channels["analog"][name]["scale"]
                )
            elif name[0] == 'D':
                data[name].extend(
                    block["binary"][name]
                )
            # and ignore all other keys (like 'time')

        if count == 1:
            print(block["time"][0], start_time)
            print("start time: ", data["time"])

        if count % 1000 == 0:
            print(count)

    # Convert data to numpy array, easier to manipulate en-masse
    for name in data.keys():
        data[name] = np.array(data[name])

    return data


def plot(names, time, data):
    _, axes = plt.subplots(len(names), sharex=True)#, figsize=(16.0 / 3, 3))
    spans = []
    if len(names) == 1:
        axes = [axes]
    for name,ax in zip(names,axes):
        ax.plot(time, data[name])
        ax.grid()
        def onselect(name_, xmin, xmax):
            ixmin, ixmax = np.searchsorted(time, [xmin, xmax])
            print(name_, ixmin, ixmax, xmin, xmax)
            import scipy.integrate
            time_interest = time[ixmin: ixmax]
            data_interest = data[name_][ixmin: ixmax]
            out = scipy.integrate.trapezoid(data_interest, time_interest)
            print("(start, end): ({}, {})".format(data[name_][ixmin], data[name_][ixmax-1]))
            print("integral trapezoid: ", out)
            out = scipy.integrate.simpson(data_interest, time_interest)
            print("integral simpson: ", out)
            print("mean: ", np.mean(data_interest))
            print('time delta:', xmax - xmin)

        func = partial(onselect, name)
        ax.margins(x=0)
        spans.append(SpanSelector(ax, func, "horizontal", useblit=True, interactive=True, drag_from_anywhere=True))
        print(ax.margins())

    plt.show()

def main():
    data = {}
    with RLD(sys.argv[1]) as rld:
        print(rld.metadata)
        print(rld.channels)
        data.update(extract_all_rld_data(rld))

    # FIXME I need to make a better way to allow selection of what to plot
    final_time = data["time"]
    #data['P1'] = data['V1'] * data['I2']
    data['P1'] = data['V1'] * data['I1']
    data['P2'] = data['V2'] * data['I2']
    #plot(['V1', 'V2', 'V3', 'V4', 'I1', 'I2', 'P1'], final_time, data)
    #plot(['V1', 'V2', 'V3', 'I1', 'I2', 'P1', 'P2'], final_time, data)
    plot(['V1', 'V2', 'V3', 'V4', 'I1', 'I2', 'P1', 'P2'], final_time, data)
    #plot(['V2'], final_time, data)
    #_, axes = plt.subplots(6, sharex=True, figsize=(16.0 / 3, 3))
    #axes[0].plot(final_time, data["V1"])
    #axes[1].plot(final_time, data["V2"])
    #axes[2].plot(final_time, data["V3"])
    #axes[3].plot(final_time, data["V4"])
    #axes[4].plot(final_time, (data["I1"]))
    #axes[5].plot(final_time, (data["I2"]))
    #axes[3].plot(final_time, data["V2"] * data["I2"] * 1000000)
    #axes[0].set(ylabel="Mudwatt voltage (V)")
    #axes[1].set(ylabel="Mudwatt power (uW)")
    #axes[2].set(ylabel="Supercap Voltage (V)")
    #axes[3].set(ylabel="Supercap power (uW)")
    #axes[0].grid()
    #axes[1].grid()
    #axes[2].grid()
    #axes[3].grid()
    #axes[4].grid()
    #axes[5].grid()
    #axes[3].grid()
    #print("mean power: ", np.mean(data["V1"] * data["I2"] * 1000000))
    print(np.mean(data['V2']))
    #print(np.mean(data['I1']))
    #print(np.mean(data['I1'] * np.mean(data['V1'])))
    #plt.show()


if __name__ == "__main__":
    main()
