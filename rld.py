"""Module providing routines to parse RocketLogger RLD binary files.
"""

import sys
import math
from typing import BinaryIO, Iterator, TypedDict
from collections import OrderedDict
from types import TracebackType
import numpy as np
import numpy.typing as npt


class RLDChannel(TypedDict):
    """Typed dict representing the metadata for a signle data channel"""

    unit: int
    scale: int
    data_size: int
    valid_data_link: int
    name: str
    index: int


class RLDMetadata(TypedDict):
    """Typed dict representing the RLD metadata in the header"""

    magic: str
    version: int
    length: int
    block_size: int
    block_count: int
    sample_count: int
    sampling_rate: int
    mac: str
    start_time_s: int
    start_time_ns: int
    comment_length: int
    binary_channel_count: int
    analog_channel_count: int
    comment: str


class RLDBlock(TypedDict):
    """Typed dict representing an RLD data block"""

    time: npt.NDArray[np.int64]
    mono_time: npt.NDArray[np.int64]
    binary: dict[str, npt.NDArray[np.int32]]
    analog: dict[str, npt.NDArray[np.int64]]


class RLD:
    """Represents an RLD file."""

    def __init__(self, filename: str):
        """Initialize RLD object.

        Keyword arguments:
        filename - Path to the RLD file to parse and read.
        """
        self.filename = filename
        # Disable the pylint warning, as we're using open on purpose here. This
        # way a user can either manage openning and closing the file on their
        # own, or they can use RLD as a context manager
        self.file: BinaryIO = open(
            self.filename, "rb"
        )  # pylint: disable=consider-using-with
        self.metadata, self.channels = self._extract_metadata()

    def __enter__(self) -> "RLD":
        return self

    def __exit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: TracebackType | None,
    ) -> None:
        self.close()

    def __iter__(self) -> Iterator[RLDBlock]:
        """Returns an iterable over all the blocks in the RLD file."""
        return self._yield_block()

    def close(self) -> None:
        """Close the RLD file."""
        self.file.close()

    def _extract_metadata(
        self,
    ) -> tuple[RLDMetadata, dict[str, OrderedDict[str, RLDChannel]]]:
        """Parses the RLD file metadata and channel metadata."""

        def get_mac(mac: bytes) -> str:
            return f"{mac[0]:02x}:{mac[1]:02x}:{mac[2]:02x}:{mac[3]:02x}:{mac[4]:02x}:{mac[5]:02x}"

        metadata: RLDMetadata = {
            "magic": self.file.read(4).decode("utf-8"),
            "version": int.from_bytes(self.file.read(2), "little"),
            "length": int.from_bytes(self.file.read(2), "little"),
            "block_size": int.from_bytes(self.file.read(4), "little"),
            "block_count": int.from_bytes(self.file.read(4), "little"),
            "sample_count": int.from_bytes(self.file.read(8), "little"),
            "sampling_rate": int.from_bytes(self.file.read(2), "little"),
            "mac": get_mac(self.file.read(6)),
            "start_time_s": int.from_bytes(self.file.read(8), "little", signed=True),
            "start_time_ns": int.from_bytes(self.file.read(8), "little", signed=True),
            "comment_length": int.from_bytes(self.file.read(4), "little"),
            "binary_channel_count": int.from_bytes(self.file.read(2), "little"),
            "analog_channel_count": int.from_bytes(self.file.read(2), "little"),
            "comment": "",
        }
        metadata["comment"] = self.file.read(metadata["comment_length"]).decode("utf-8")

        def extract_channels(rld: BinaryIO, count: int) -> OrderedDict[str, RLDChannel]:
            channels: OrderedDict[str, RLDChannel] = OrderedDict()
            for i in range(count):
                channel: RLDChannel = {
                    "unit": int.from_bytes(self.file.read(4), "little", signed=True),
                    "scale": int.from_bytes(self.file.read(4), "little", signed=True),
                    "data_size": int.from_bytes(self.file.read(2), "little"),
                    "valid_data_link": int.from_bytes(self.file.read(2), "little"),
                    "name": self.file.read(16).decode("utf-8").strip("\0"),
                    "index": i,
                }

                channels[channel["name"]] = channel
            return channels

        binary_channels: OrderedDict[str, RLDChannel] = extract_channels(
            self.file, metadata["binary_channel_count"]
        )
        analog_channels: OrderedDict[str, RLDChannel] = extract_channels(
            self.file, metadata["analog_channel_count"]
        )

        channels: dict[str, OrderedDict[str, RLDChannel]] = {}
        channels["binary"] = binary_channels
        channels["analog"] = analog_channels

        return metadata, channels

    def _yield_block(
        self, start_block: int = 0, end_block: int = -1
    ) -> Iterator[RLDBlock]:
        """Yields a block from the RLD binary file.

        Keyword arguments:
        start_block - The block to start from
        end_block - One past the final block to parse. A negative number here means
                    to continue yielding until all blocks are parsed.
        """

        # If the file isn't opened, or we get a None, just bail with an empty generator
        if self.file.closed:
            yield from ()

        if end_block < 0:
            end_block = self.metadata["block_count"]

        # Seek to the start of block data
        offset = (
            56
            + self.metadata["comment_length"]
            + 28
            * (
                self.metadata["binary_channel_count"]
                + self.metadata["analog_channel_count"]
            )
        )

        sample_size = 4 * int(math.ceil(self.metadata["binary_channel_count"] / 32))
        for channel in self.channels["analog"].values():
            sample_size += channel["data_size"]

        offset += (32 + self.metadata["block_size"] * sample_size) * start_block
        self.file.seek(offset)

        for _ in range(start_block, end_block):
            block = self._extract_block()
            yield block

    def _extract_block(self) -> RLDBlock:
        """Extracts a block from the current position in the RLD file."""

        # Extract block time from RLD
        def get_time(file: BinaryIO) -> tuple[float, float]:
            seconds: int = int.from_bytes(file.read(8), "little", signed=True)
            nanoseconds: int = int.from_bytes(file.read(8), "little", signed=True)
            time_: float = seconds + nanoseconds * 10**-9
            mono_s: int = int.from_bytes(file.read(8), "little", signed=True)
            mono_ns: int = int.from_bytes(file.read(8), "little", signed=True)
            mono_time: float = mono_s + mono_ns * 10**-9
            return time_, mono_time

        base_time, base_mono_time = get_time(self.file)

        time: list[float] = []
        mono_time: list[float] = []
        binary: OrderedDict[str, list[int]] = OrderedDict()
        analog: OrderedDict[str, list[int]] = OrderedDict()

        binary.update({channel: [] for channel in self.channels["binary"].keys()})
        analog.update({channel: [] for channel in self.channels["analog"].keys()})

        for sample in range(self.metadata["block_size"]):
            # Assign a time to each sample, based on its position in the current block
            sample_time = sample / self.metadata["sampling_rate"]
            time.append(base_time + sample_time)
            mono_time.append(base_mono_time + sample_time)

            # Process binary channels (first in the file)
            if self.metadata["binary_channel_count"] != 0:
                data = 0
                for channel_index, channel in enumerate(self.channels["binary"].values()):
                    if channel_index % 32 == 0:
                        data = int.from_bytes(self.file.read(4), "little", signed=False)
                    binary[channel["name"]].append((data >> (channel_index % 32)) & 0x01)
            # And now do the analog channels (follow binary channels if they exist)
            for channel in self.channels["analog"].values():
                data = int.from_bytes(
                    self.file.read(channel["data_size"]),
                    "little",
                    signed=True,
                )
                analog[channel["name"]].append(data)

        # Make each block a dictionary of time, mono_time, and one list per channel
        return {
            "time": np.array(time, dtype=np.float64),
            "mono_time": np.array(mono_time, dtype=np.float64),
            "binary": OrderedDict(
                (key, np.array(value, dtype=np.float64))
                for key, value in binary.items()
            ),
            "analog": OrderedDict(
                (key, np.array(value, dtype=np.float64))
                for key, value in analog.items()
            ),
        }


def main() -> None:
    """Test main function, returns the metadata of the RLD file"""
    for file_ in sys.argv[1:]:
        print(file_)
        with RLD(file_) as rld:
            print(rld.metadata)
            print(rld.channels)

            block = next(iter(rld))
            print(block)


if __name__ == "__main__":
    main()
